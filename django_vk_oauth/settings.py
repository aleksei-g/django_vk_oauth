import os
from configurations import Configuration, values


class Base(Configuration):

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    SECRET_KEY = values.SecretValue(
        environ_prefix=None,
        environ_name='SECRET_KEY',
    )

    DEBUG = values.BooleanValue(
        default=False,
        environ_prefix=None,
        environ_name='DEBUG',
    )

    ALLOWED_HOSTS = values.ListValue(
        default=['localhost', '127.0.0.1'],
        environ_prefix=None,
        environ_name='ALLOWED_HOSTS',
    )

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
        'allauth',
        'allauth.account',
        'allauth.socialaccount',
        'providers.vk',
        'users.apps.UsersConfig',
    ]

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    ROOT_URLCONF = 'django_vk_oauth.urls'

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [
                os.path.join(BASE_DIR, 'templates'),
                os.path.join(BASE_DIR, 'templates', 'allauth'),
            ],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    'django.template.context_processors.request',
                ],
            },
        },
    ]

    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
    )

    WSGI_APPLICATION = 'django_vk_oauth.wsgi.application'

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    SOCIALACCOUNT_PROVIDERS = {
        'vk': {
            'SCOPE': ['friends'],
        }
    }

    LANGUAGE_CODE = 'en-us'

    TIME_ZONE = 'UTC'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    SITE_ID = 1

    LOGIN_REDIRECT_URL = 'user-friends'

    STATIC_URL = '/static/'

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, 'staticfiles'),
    ]

    STATIC_ROOT = os.path.join(BASE_DIR, 'static')


class Dev(Base):
    DEBUG = values.BooleanValue(
        default=True,
        environ_prefix=None,
        environ_name='DEBUG',
    )

class Prod(Base):
    ALLOWED_HOSTS = values.ListValue(
        environ_prefix=None,
        environ_name='ALLOWED_HOSTS',
    )
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': values.Value(
                environ_prefix=None,
                environ_name='DB_NAME',
            ),
            'USER': values.Value(
                environ_prefix=None,
                environ_name='DB_USER',
            ),
            'PASSWORD': values.Value(
                environ_prefix=None,
                environ_name='DB_PASSWORD',
                default='',
            ),
            'HOST': values.Value(
                environ_prefix=None,
                environ_name='DB_HOST',
                default='localhost',
            ),
            'PORT': values.Value(
                environ_prefix=None,
                environ_name='DB_PORT',
                default='',
            ),
        }
    }
    MIDDLEWARE = Base.MIDDLEWARE
    MIDDLEWARE.insert(
        MIDDLEWARE.index('django.middleware.security.SecurityMiddleware') + 1,
        'whitenoise.middleware.WhiteNoiseMiddleware',
    )
    CSRF_COOKIE_SECURE = True
    SESSION_COOKIE_SECURE = True
    X_FRAME_OPTIONS = 'DENY'
    SECURE_HSTS_SECONDS = 1
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_BROWSER_XSS_FILTER = True
    SECURE_SSL_REDIRECT = True

