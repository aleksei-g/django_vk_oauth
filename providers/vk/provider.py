from allauth.socialaccount.providers.vk.provider import VKProvider


class CustomVKProvider(VKProvider):
    def extract_uid(self, data):
        return str(data['id'])


provider_classes = [CustomVKProvider]
