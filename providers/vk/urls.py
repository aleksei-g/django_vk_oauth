from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import CustomVKProvider


urlpatterns = default_urlpatterns(CustomVKProvider)
