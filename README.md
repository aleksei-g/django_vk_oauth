# Django Vk oauth

This Django project implements VK oauth authorization and displays 5 random friends of the user.
Application is available by url [https://django-vk-oauth.herokuapp.com/](https://django-vk-oauth.herokuapp.com/).

Project prepared for deployment on [Heroku](https://www.heroku.com/).

## Local installing
1. Create a directory and go to it. Clone project into this folder:

    ```
    $ mkdir project_folder
    $ cd project_folder
    $ git clone https://gitlab.com/aleksei-g/django_vk_oauth.git .
    ```

2. Create a virtual environment with Python 3.6+ and install all required packeges:

    ```
    $ python3.6 -m venv virtualenv_path
    $ source virtualenv_path/bin/activate
    ```
    On dev machine install:
     ```
    (virtualenv_path) $ pip install -r requirements.dev.txt
    ```
    On production server install:
     ```
    (virtualenv_path) $ pip install -r requirements.txt
    ```

3. Set the value of the environment variables:
 * **DJANGO_CONFIGURATION** (required) - To run application in development mode, use with the value `Dev`. To run on the server used the value `Prod`. Default value is `Prod`.
 * **SECRET_KEY** (required) - A secret key for a particular Django installation. This is used to provide cryptographic signing, and should be set to a unique, unpredictable value.
 * **ALLOWED_HOSTS** (required in Prod mode) - Comma-separated list of strings representing the host/domain names that this Django site can serve. Default value: `'localhost', '127.0.0.1'`.
 * **DEBUG** (optional) - A boolean that turns on/off debug mode. Default value 'False'.
 * **DB_NAME** (required in Prod mode) - The name of the database to use.
 * **DB_USER** (required in Prod mode) - The username to use when connecting to the database.
 * **DB_PASSWORD** (required in Prod mode) - The password to use when connecting to the database.
 * **DB_HOST** (required in Prod mode) - Which host to use when connecting to the database.
 * **DB_PORT**  (required in Prod mode) - The port to use when connecting to the database. 
 
## Before running
Go to [https://vk.com/editapp?act=create](https://vk.com/editapp?act=create) and register your application for authorization.

Specify the following parameters:
* **Website address**: `https://site.com`
* **Base Domain**: `site.com`
* **Trusted redirect URI** is based on this principle: `http://[domain]/accounts/vk/login/callback/`

After completing all the steps you will receive the `application ID` and `secret key`. You will need this information to configure the oauth provider on the django admin page.

## Run and using
Create database and migrate:
```
(virtualenv_path) $ python3 manage.py migrate
```
Run the application use the command:
```
(virtualenv_path) $ python3 manage.py runserver localhost:8000
```
The application will be available by link [http://localhost:8000/](http://localhost:8000/).

Create superuser:
```
(virtualenv_path) $ python3 manage.py createsuperuser
```

Go to the [admin page django](http://localhost:8000/admin), open **"Sites"** settings and change existing item, specifying a domain equal to the domain from the VK application settings.

Then open the **"Social applications"** page, push "add social application" button and specify the `Client ID` and `Secret key` parameters obtained during registration of the VK application.

The application is configured. Now you can login using VK oauth authorization from the page [http://localhost:8000/accounts/login/](http://localhost:8000/accounts/login/)  and get 5 random friends on the page [http://localhost:8000/](http://localhost:8000/).

