from django.urls import path
from .views import random_friends


urlpatterns = [
    path('', random_friends, name='user-friends'),
]