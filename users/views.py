from django.shortcuts import render
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.models import SocialToken
import requests


def random_friends(request):
    context = {'title': '5 Random Friends'}
    if request.user.is_authenticated:
        vk_social_account= SocialAccount.objects.filter(
            user_id=request.user.id,
            provider='vk',
        )
        if vk_social_account.exists():
            vk_id = vk_social_account[0].uid
            token = SocialToken.objects.filter(
                account__user=request.user,
                account__provider='vk',
            ).first()
            params = {
                'v': '5.95',
                'access_token': token,
                'user_id': vk_id,
                'order': 'random',
                'count': '5',
                'fields': 'domain,photo_100',
            }
            resp = requests.get('https://api.vk.com/method/friends.get',
                                params=params)
            resp.raise_for_status()
            friends = resp.json().get('response', {}).get('items')
            context['friends'] = friends
    return render(request, 'users/user_friends.html', context)